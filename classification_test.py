import numpy as np
from keras.models import *
from keras.layers import Input, Flatten, Dense, merge, Conv2D, MaxPooling2D, UpSampling2D, Dropout, Cropping2D, Concatenate, Activation
from keras.applications.vgg16 import VGG16
from keras.layers.normalization import BatchNormalization
from keras.utils.np_utils import to_categorical
from keras import backend as K
K.tensorflow_backend._get_available_gpus()
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from utils import plot_confusion_matrix
import pandas as pd

root_path = "D:/Program_Skin_Cancer/OUTPUT/classification/data_train_class_with_model_seg/"
test_images = np.load(root_path+'classification_image_test.npy')
test_labels = np.load(root_path+'classification_labels_test.npy')

#----------------------------------------------------------------
#load weights model class
#----------------------------------------------------------------
test_mean = np.mean(test_images, axis=(0, 1, 2, 3))
test_std = np.std(test_images, axis=(0, 1, 2, 3))
test_images = (test_images - test_mean)/(test_std + 1e-7)

input_shape = (128,128,3)
num_classes = 2
tmodel_base = VGG16(weights='imagenet',include_top=False,input_shape=input_shape)
tmodel = Sequential()
tmodel.add(tmodel_base)
tmodel.add(BatchNormalization())
tmodel.add(Dropout(0.50))
tmodel.add(Flatten())
tmodel.add(Dense(512, activation='relu'))
tmodel.add(BatchNormalization())
tmodel.add(Dropout(0.25))
tmodel.add(Dense(num_classes, activation='softmax',name='output_layer'))
tmodel.summary()
tmodel.load_weights(root_path+'VGG16_weight_classification_epochs60_bs4.hdf5')
sample_predictions = tmodel.predict(test_images)

predicted_labels = np.zeros(sample_predictions.shape[0])

for i in range(sample_predictions.shape[0]):
    if sample_predictions[i][0] > 0.5:
        predicted_labels[i] = 0
    else:
        predicted_labels[i] = 1

# Compute confusion matrix
cnf_matrix = confusion_matrix(test_labels, predicted_labels)
np.set_printoptions(precision=2)
class_names = ['melanoma', 'others']
# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=class_names, title='Confusion matrix, without normalization')

# Plot normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True, title='Normalized confusion matrix')

data = pd.read_csv(root_path+'VGG16_logger_classification_epochs60_bs4.csv')
fig, ax = plt.subplots(2,1)
ax[0].plot(data['loss'], color='b', label="training loss")
ax[0].plot(data['val_loss'], color='r', label="Validation loss", axes=ax[0])
legend = ax[0].legend(loc='best', shadow=True)

ax[1].plot(data['acc'], color='b', label="training acc")
ax[1].plot(data['val_acc'], color='r', label="Validation acc",axes=ax[1])
legend = ax[1].legend(loc='best', shadow=True)
# plt.figure()
# plt.plot(data['loss'], color='b', label="training loss")
# plt.plot(data['val_loss'], color='r', label="testing loss")
# legend = plt.legend(loc='best', shadow=True)
plt.show()

from sklearn import metrics
print("DecisionTrees's Accuracy: ", metrics.accuracy_score(test_labels, predicted_labels))
from sklearn.metrics import log_loss
print("DecisionTrees's log_loss: ",log_loss(test_labels, predicted_labels))