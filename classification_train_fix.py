###########################################################
#
# DeepCancer - Skin cancer detection
# Description:
# - classification.py
# - create date: 2019-2-16
#
############################################################


import numpy as np
import argparse
import os
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, CSVLogger
from keras.models import *
from keras.layers import Input, Flatten, Dense, merge, Conv2D, MaxPooling2D, UpSampling2D, Dropout, Cropping2D, Concatenate, Activation
from keras.applications.vgg16 import VGG16
from keras.layers.normalization import BatchNormalization
from keras.utils.np_utils import to_categorical
from keras import backend as K
K.tensorflow_backend._get_available_gpus()
from tensorflow.python.client import device_lib
from keras.preprocessing.image import ImageDataGenerator

class Classification():
    def __init__(self, batch_size, epochs, data_path, image_train_dir, mask_train_dir, image_val_dir, mask_val_dir, num_classes,
                 weight_save_prefix='VGG16_weight_classification.hdf5', save_csv_logger='VGG16_logger_classification.csv', seed=1):
        self.batch_size = batch_size
        self.data_path = data_path
        self.epochs = epochs
        self.image_train_dir = image_train_dir
        self.mask_train_dir = mask_train_dir
        self.image_val_dir = image_val_dir
        self.mask_val_dir = mask_val_dir
        self.weight_save_prefix = weight_save_prefix
        self.save_csv_logger = save_csv_logger
        self.seed = seed
        self.num_classes = num_classes

    def _Model_VGG16_classification(self):
        # training classification the model VGG16
        tmodel_base = VGG16(weights='imagenet', include_top=False, input_shape=(128,128,3))
        tmodel = Sequential()
        tmodel.add(tmodel_base)
        tmodel.add(BatchNormalization())
        tmodel.add(Dropout(0.50))
        tmodel.add(Flatten())
        tmodel.add(Dense(512, activation='relu'))
        tmodel.add(BatchNormalization())
        tmodel.add(Dropout(0.25))
        tmodel.add(Dense(self.num_classes, activation='softmax', name='output_layer'))
        tmodel.summary()
        tmodel.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])
        lr_reducer = ReduceLROnPlateau(factor=0.5, cooldown=0, patience=6, min_lr=0.5e-6)
        csv_logger = CSVLogger(save_csv_logger)
        model_checkpoint = ModelCheckpoint(weight_save, monitor='val_loss', verbose=1, save_best_only=True)
        tmodel.fit(image_train, mask_train,
                             batch_size=self.batch_size,
                             epochs=self.epochs,
                             validation_data=(image_val, mask_val),
                             verbose=1, shuffle=True, callbacks=[lr_reducer, csv_logger, model_checkpoint])

def _get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data-path', default='D:/Program_Skin_Cancer/OUTPUT/classification/data_train_class_with_model_seg/', type=str,
                        help='The path to the training data')
    parser.add_argument('--image-train-dir', default='classification_image_train.npy', type=str,
                        help='The path to the images train')
    parser.add_argument('--mask-train-dir', default='classification_labels_train.npy', type=str,
                        help='The path to the masks train')
    parser.add_argument('--image-val-dir', default='classification_image_val.npy', type=str,
                        help='The path to the images val')
    parser.add_argument('--mask-val-dir', default='classification_labels_val.npy', type=str,
                        help='The path to the masks val')
    parser.add_argument('--weight-save-prefix', default='VGG16_weight_classification_epochs100_bs16_20%dropout.hdf5', type=str,
                        help='The name to save weight prefix')
    parser.add_argument('--batch-size', default=16, type=int,
                        help='The batch size of the ')
    parser.add_argument('--epochs', default=100, type=int,
                        help='The epochs of the ')
    parser.add_argument('--save-csv-logger', default='VGG16_logger_classification_epochs100_bs16_20%dropout.csv', type=str,
                        help='The name to save csv logger')
    parser.add_argument('--num-classes', default=2, type=int,
                        help='The num_classes')
    return parser.parse_args()


if __name__ == "__main__":
    print(device_lib.list_local_devices())
    # ================= get the arguments ====================
    args = _get_args()
    save_csv_logger = os.path.join(args.data_path, args.save_csv_logger)
    weight_save = os.path.join(args.data_path, args.weight_save_prefix)
    path_root = args.data_path
    image_train_dir = os.path.join(args.data_path, args.image_train_dir)
    print(image_train_dir)
    image_train = np.load(image_train_dir)
    train_mean = np.mean(image_train, axis=(0, 1, 2, 3))
    train_std = np.std(image_train, axis=(0, 1, 2, 3))
    image_train = (image_train - train_mean) / (train_std + 1e-7)

    mask_train_dir = os.path.join(args.data_path, args.mask_train_dir)
    mask_train = np.load(mask_train_dir)
    mask_train = to_categorical(mask_train, num_classes=2)

    image_val_dir = os.path.join(args.data_path, args.image_val_dir)
    image_val = np.load(image_val_dir)
    val_mean = np.mean(image_val, axis=(0, 1, 2, 3))
    val_std = np.std(image_val, axis=(0, 1, 2, 3))

    image_val = (image_val - val_mean) / (val_std + 1e-7)

    mask_val_dir = os.path.join(args.data_path, args.mask_val_dir)
    mask_val = np.load(mask_val_dir)
    mask_val = to_categorical(mask_val, num_classes=2)

    # augmentation
    augs = ImageDataGenerator(
        featurewise_center=True,
        featurewise_std_normalization=True,
        rotation_range=0.2,
        width_shift_range=0.05,
        height_shift_range=0.05,
        horizontal_flip=True)

    augs.fit(image_train)

    # annealer
    annealer = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=3, min_lr=0.001)
    # ========================================================
    # create the instance of Segmentation class
    aug = Classification(args.batch_size,
                    args.epochs,
                    args.data_path,
                    args.image_train_dir,
                    args.mask_train_dir,
                    args.image_val_dir,
                    args.mask_val_dir,
                    args.num_classes,
                    args.weight_save_prefix,
                    args.save_csv_logger)

    trainning = aug._Model_VGG16_classification()
print('')
print('#==============================================#')
print('Congratulations on your successful training')
print('#==============================================#')