from utils import *
import argparse
from scipy import ndimage
from Unet_Model import UNetModel
from scipy import misc
import cv2

parser = argparse.ArgumentParser()
parser.add_argument("--data-dir", type=str, default="D:/Program_Skin_Cancer/DATA/",
                    help="path to the dataset")
parser.add_argument("--save-dir", type=str, default="D:/Program_Skin_Cancer/OUTPUT/",
                    help="path to save dataset")
parser.add_argument("--load-weight-dir", type=str, default="segmentation/model_weight/Unet_weight_segmentation_epochs100_bs4.hdf5",
                    help="path to save dataset")

parser.add_argument("--classification-labels-train", type=str, default='classification/data_train_class_with_seg_origin/classification_labels_train.npy',
                    help="path to the classification labels train")
parser.add_argument("--classification-images-train", type=str, default='classification/data_train_class_with_seg_origin/classification_image_train.npy',
                    help="path to the classification image train")

parser.add_argument("--classification-labels-val", type=str, default='classification/data_train_class_with_seg_origin/classification_labels_val.npy',
                    help="path to the classification labels val")
parser.add_argument("--classification-images-val", type=str, default='classification/data_train_class_with_seg_origin/classification_image_val.npy',
                    help="path to the classification image val")

parser.add_argument("--classification-labels-test", type=str, default='classification/data_train_class_with_seg_origin/classification_labels_test.npy',
                    help="path to the classification labels test")
parser.add_argument("--classification-images-test", type=str, default='classification/data_train_class_with_seg_origin/classification_image_test.npy',
                    help="path to the classification image test")

agrs = parser.parse_args()

root_path = agrs.data_dir

root_path_save = agrs.save_dir
weight_dir = os.path.join(agrs.save_dir, agrs.load_weight_dir)
classification_labels_train = agrs.classification_labels_train
classification_images_train = agrs.classification_images_train

classification_labels_val = agrs.classification_labels_val
classification_images_val = agrs.classification_images_val

classification_labels_test = agrs.classification_labels_test
classification_images_test = agrs.classification_images_test

#==data train_image_ classification ==============
filenames_train = get_filenames(root_path+"Data_classification/train/")
filenames_train.sort(key=natural_key)
image_train = []
for file in filenames_train:
    image_train.append(ndimage.imread(root_path+"Data_classification/train/"+file))
image_train = np.array(image_train)

#==data train_label_ classification ==============
classification_labels = np.zeros((len(image_train)))
i = 0
for file in filenames_train:
    if os.path.exists(root_path+"melanoma/"+file):
        classification_labels[i]=0
    else:
        classification_labels[i]=1
    i+=1

labels_train = classification_labels
np.save(root_path_save+classification_labels_train, labels_train)

#==data val_image_ classification ==============
filenames_val = get_filenames(root_path+"Data_classification/val/")
filenames_val.sort(key=natural_key)
image_val = []
for file in filenames_val:
    image_val.append(ndimage.imread(root_path+"Data_classification/val/"+file))
image_val = np.array(image_val)

#==data test_label_ classification ==============
classification_labels = np.zeros((len(image_val)))
i = 0
for file in filenames_val:
    if os.path.exists(root_path+"melanoma/"+file):
        classification_labels[i]=0
    else:
        classification_labels[i]=1
    i+=1
labels_val = classification_labels
np.save(root_path_save+classification_labels_val, labels_val)

#==data test_image_ classification ==============
filenames_test = get_filenames(root_path+"Data_classification/test/")
filenames_test.sort(key=natural_key)
image_test = []
for file in filenames_test:
    image_test.append(ndimage.imread(root_path+"Data_classification/test/"+file))
image_test = np.array(image_test)

#==data val_label_ classification ==============
classification_labels = np.zeros((len(image_test)))
i = 0
for file in filenames_test:
    if os.path.exists(root_path+"melanoma/"+file):
        classification_labels[i]=0
    else:
        classification_labels[i]=1
    i+=1
labels_test = classification_labels
np.save(root_path_save+classification_labels_test, labels_test)

path = "D:/Program_Skin_Cancer/DATA/Data_classification/data_segmentated/gt/"
#== segmentation data train ==================================================================
# load weights model
model = UNetModel.get_unet_model_seg((128, 128, 3))
model.load_weights(weight_dir)
ground_truth_images = []
for jj in range(len(image_train)):
    sample_predictions = model.predict(image_train[jj].reshape((1, 128, 128, 3)))
    sample_predictions = sample_predictions.reshape((128, 128))
    sample_predictions = sample_predictions > 0.5
    sample_predictions = np.array(sample_predictions, dtype=np.uint8)
    ground_truth_images.append(sample_predictions)

segmented_images = np.copy(image_train)
x, y, z = segmented_images[0].shape
i=0
for file in filenames_train:
    if os.path.exists(path + file):
        ground_truth_images1 = ndimage.imread(path + file)
        ret, ground_truth_images1 = cv2.threshold(ground_truth_images1, 127, 255, cv2.THRESH_BINARY)
        gt_labels_binary = np.array(ground_truth_images1)
        np.unique(gt_labels_binary[0])
        gt_labels_binary = gt_labels_binary / 255
        np.unique(gt_labels_binary[0])
        ground_truth_origin = gt_labels_binary
        for j in range(x):
            for k in range(y):
                for l in range(z):
                    segmented_images[i][j][k][l] = image_train[i][j][k][l] if ground_truth_origin[j][k] == 1 else 0
        # plt.figure()
        # plt.imshow(segmented_images[i])
        # plt.show()
        misc.imsave(root_path_save + "classification/data_train_class_with_seg_origin/train_seg_origin/" + file, segmented_images[i])
    else:
        for j in range(x):
            for k in range(y):
                for l in range(z):
                    segmented_images[i][j][k][l] = image_train[i][j][k][l] if ground_truth_images[i][j][k] == 1 else 0
        misc.imsave(root_path_save + "classification/data_train_class_with_seg_origin/train_seg_origin/" + file, segmented_images[i])
    i = i+1
np.save(root_path_save+classification_images_train, segmented_images)

#== segmentation data val ==================================================================
# load weights model
model = UNetModel.get_unet_model_seg((128, 128, 3))
model.load_weights(weight_dir)
ground_truth_images_val = []
for ja in range(len(image_val)):
    sample_predictions = model.predict(image_val[ja].reshape((1, 128, 128, 3)))
    sample_predictions = sample_predictions.reshape((128, 128))
    sample_predictions = sample_predictions > 0.5
    sample_predictions = np.array(sample_predictions, dtype=np.uint8)
    ground_truth_images_val.append(sample_predictions)

segmented_images_val = np.copy(image_val)
xv, yv, zv = segmented_images_val[0].shape
iv=0
for file in filenames_val:
    if os.path.exists(path + file):
        ground_truth_images1 = ndimage.imread(path + file)
        ret, ground_truth_images1 = cv2.threshold(ground_truth_images1, 127, 255, cv2.THRESH_BINARY)
        gt_labels_binary = np.array(ground_truth_images1)
        np.unique(gt_labels_binary[0])
        gt_labels_binary = gt_labels_binary / 255
        np.unique(gt_labels_binary[0])
        ground_truth_origin = gt_labels_binary
        for jv in range(xv):
            for kv in range(yv):
                for lv in range(zv):
                    segmented_images_val[iv][jv][kv][lv] = image_val[iv][jv][kv][lv] if ground_truth_origin[jv][kv] == 1 else 0
        misc.imsave(root_path_save + "classification/data_train_class_with_seg_origin/val_seg_origin/" + file, segmented_images_val[iv])
    else:
        for jv in range(xv):
            for kv in range(yv):
                for lv in range(zv):
                    segmented_images_val[iv][jv][kv][lv] = image_val[iv][jv][kv][lv] if ground_truth_images_val[iv][jv][kv] == 1 else 0
        misc.imsave(root_path_save + "classification/data_train_class_with_seg_origin/val_seg_origin/" + file, segmented_images_val[iv])
    iv = iv+1
np.save(root_path_save+classification_images_val, segmented_images_val)

#== segmentation data test ==================================================================
# load weights model
model = UNetModel.get_unet_model_seg((128, 128, 3))
model.load_weights(weight_dir)
ground_truth_images_test = []
for jb in range(len(image_test)):
    sample_predictions = model.predict(image_test[jb].reshape((1, 128, 128, 3)))
    sample_predictions = sample_predictions.reshape((128, 128))
    sample_predictions = sample_predictions > 0.5
    sample_predictions = np.array(sample_predictions, dtype=np.uint8)
    ground_truth_images_test.append(sample_predictions)

segmented_images_test = np.copy(image_test)
xv, yv, zv = segmented_images_test[0].shape

iv=0
for file in filenames_test:
    if os.path.exists(path + file):
        ground_truth_images1 = ndimage.imread(path + file)
        ret, ground_truth_images1 = cv2.threshold(ground_truth_images1, 127, 255, cv2.THRESH_BINARY)
        gt_labels_binary = np.array(ground_truth_images1)
        np.unique(gt_labels_binary[0])
        gt_labels_binary = gt_labels_binary / 255
        np.unique(gt_labels_binary[0])
        ground_truth_origin = gt_labels_binary
        for jv in range(xv):
            for kv in range(yv):
                for lv in range(zv):
                    segmented_images_test[iv][jv][kv][lv] = image_test[iv][jv][kv][lv] if ground_truth_origin[jv][kv] == 1 else 0
        misc.imsave(root_path_save + "classification/data_train_class_with_seg_origin/test_seg_origin/" + file, segmented_images_test[iv])
    else:
        for jv in range(xv):
            for kv in range(yv):
                for lv in range(zv):
                    segmented_images_test[iv][jv][kv][lv] = image_test[iv][jv][kv][lv] if ground_truth_images_test[iv][jv][kv] == 1 else 0
        misc.imsave(root_path_save + "classification/data_train_class_with_seg_origin/test_seg_origin/" + file, segmented_images_test[iv])
    iv = iv+1
np.save(root_path_save+classification_images_test, segmented_images_test)