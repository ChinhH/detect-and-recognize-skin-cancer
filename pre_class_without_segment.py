from utils import *
import argparse
from scipy import ndimage

parser = argparse.ArgumentParser()
parser.add_argument("--data-dir", type=str, default="D:/Program_Skin_Cancer/DATA/",
                    help="path to the dataset")
parser.add_argument("--save-dir", type=str, default="D:/Program_Skin_Cancer/OUTPUT/",
                    help="path to save dataset")

parser.add_argument("--classification-labels-train", type=str, default='classification/data_train_class_without_model_seg/classification_labels_train.npy',
                    help="path to the classification labels train")
parser.add_argument("--classification-images-train", type=str, default='classification/data_train_class_without_model_seg/classification_image_train.npy',
                    help="path to the classification image train")

parser.add_argument("--classification-labels-val", type=str, default='classification/data_train_class_without_model_seg/classification_labels_val.npy',
                    help="path to the classification labels val")
parser.add_argument("--classification-images-val", type=str, default='classification/data_train_class_without_model_seg/classification_image_val.npy',
                    help="path to the classification image val")

parser.add_argument("--classification-labels-test", type=str, default='classification/data_train_class_without_model_seg/classification_labels_test.npy',
                    help="path to the classification labels test")
parser.add_argument("--classification-images-test", type=str, default='classification/data_train_class_without_model_seg/classification_image_test.npy',
                    help="path to the classification image test")

agrs = parser.parse_args()

root_path = agrs.data_dir

root_path_save = agrs.save_dir
classification_labels_train = agrs.classification_labels_train
classification_images_train = agrs.classification_images_train

classification_labels_val = agrs.classification_labels_val
classification_images_val = agrs.classification_images_val

classification_labels_test = agrs.classification_labels_test
classification_images_test = agrs.classification_images_test

#==data train_image_ classification ==============
filenames_train = get_filenames(root_path+"Data_classification/train/")
filenames_train.sort(key=natural_key)
image_train = []
for file in filenames_train:
    image_train.append(ndimage.imread(root_path+"Data_classification/train/"+file))
image_train = np.array(image_train)
np.save(root_path_save+classification_images_train, image_train)
#==data train_label_ classification ==============
classification_labels = np.zeros((len(image_train)))
i = 0
aa = 0
bb = 0
for file in filenames_train:
    if os.path.exists(root_path+"melanoma/"+file):
        classification_labels[i]=0
        aa = aa+1
    else:
        classification_labels[i]=1
        bb = bb+1
    i+=1

labels_train = classification_labels
np.save(root_path_save+classification_labels_train, labels_train)
print("aa=", aa)
print("bb=", bb)
#==data val_image_ classification ==============
filenames_val = get_filenames(root_path+"Data_classification/val/")
filenames_val.sort(key=natural_key)
image_val = []
for file in filenames_val:
    image_val.append(ndimage.imread(root_path+"Data_classification/val/"+file))
image_val = np.array(image_val)
np.save(root_path_save+classification_images_val, image_val)
#==data test_label_ classification ==============
classification_labels = np.zeros((len(image_val)))
i = 0
aa = 0
bb = 0
for file in filenames_val:
    if os.path.exists(root_path+"melanoma/"+file):
        classification_labels[i]=0
        aa = aa + 1
    else:
        classification_labels[i]=1
        bb = bb + 1
    i+=1
labels_val = classification_labels
np.save(root_path_save+classification_labels_val, labels_val)
print("aa=", aa)
print("bb=", bb)
#==data test_image_ classification ==============
filenames_test = get_filenames(root_path+"Data_classification/test/")
filenames_test.sort(key=natural_key)
image_test = []
for file in filenames_test:
    image_test.append(ndimage.imread(root_path+"Data_classification/test/"+file))
image_test = np.array(image_test)
np.save(root_path_save+classification_images_test, image_test)
#==data val_label_ classification ==============
classification_labels = np.zeros((len(image_test)))
i = 0
aa = 0
bb = 0
for file in filenames_test:
    if os.path.exists(root_path+"melanoma/"+file):
        classification_labels[i]=0
        aa = aa + 1
    else:
        classification_labels[i]=1
        bb = bb + 1
    i+=1
labels_test = classification_labels
np.save(root_path_save+classification_labels_test, labels_test)
print("aa=", aa)
print("bb=", bb)