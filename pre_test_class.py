from utils import *
import argparse
from scipy import ndimage

from Unet_Model import UNetModel

parser = argparse.ArgumentParser()
parser.add_argument("--data-dir", type=str, default="D:/Learnning/data_pre/Data_classification/",
                    help="path to the dataset")
parser.add_argument("--save-dir", type=str, default="D:/Learnning/data_pre/OUT/",
                    help="path to save dataset")
parser.add_argument("--load-weight-dir", type=str, default="D:/Learnning/TensorFlow/program/model_base_Resnet/OUTPUT/_model-weights/segmentation/Unet_weight_segmentation.hdf5",
                    help="path to save dataset")
agrs = parser.parse_args()

root_path = agrs.data_dir
root_path_save = agrs.save_dir
weight_dir = agrs.load_weight_dir

#==data train_image_ classification ==============
filenames_train = get_filenames(root_path+"test/")
filenames_train.sort(key=natural_key)
image_train = []
for file in filenames_train:
    image_train.append(ndimage.imread(root_path+"test/"+file))
image_train = np.array(image_train)

#==data train_label_ classification ==============
classification_labels = np.zeros((len(image_train)))
i = 0
for file in filenames_train:
    if os.path.exists('D:/Learnning/data_pre/melanoma/'+file):
        classification_labels[i]=0
    else:
        classification_labels[i]=1
    i+=1

labels_train = classification_labels
np.save(root_path_save+'classification_labels_test.npy', labels_train)

#== segmentation data train ==================================================================
# load weights model
model = UNetModel.get_unet_model_seg((128, 128, 3))
model.load_weights(weight_dir)
ground_truth_images = []
for ix in range(len(image_train)):
    sample_predictions = model.predict(image_train[ix].reshape((1, 128, 128, 3)))
    sample_predictions = sample_predictions.reshape((128, 128))
    sample_predictions = sample_predictions > 0.5
    sample_predictions = np.array(sample_predictions, dtype=np.uint8)
    ground_truth_images.append(sample_predictions)

segmented_images = np.copy(image_train)
x, y, z = segmented_images[0].shape

for i in range(len(image_train)):
    for j in range(x):
        for k in range(y):
            for l in range(z):
                segmented_images[i][j][k][l] = image_train[i][j][k][l] if ground_truth_images[i][j][k] == 1 else 0
    # misc.imsave(root_path+"DATA/segmented_images_train/segmented_"+filenames_train[i], segmented_images[i])

np.save(root_path_save+'classification_image_test.npy', segmented_images)
