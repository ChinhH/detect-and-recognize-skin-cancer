import pandas as pd
from matplotlib import pyplot as plt

data = pd.read_csv("D:/Program_Skin_Cancer/OUTPUT/segmentation/binary_data/Unet_logger_segmentation_epochs100_bs4.csv")
fig, ax = plt.subplots(2,1)
ax[0].plot(data['loss'], color='b', label="training loss")
ax[0].plot(data['val_loss'], color='r', label="test loss", axes=ax[0])
legend = ax[0].legend(loc='best', shadow=True)

ax[1].plot(data['acc'], color='b', label="training acc")
ax[1].plot(data['val_acc'], color='r', label="test acc",axes=ax[1])
legend = ax[1].legend(loc='best', shadow=True)
plt.show()