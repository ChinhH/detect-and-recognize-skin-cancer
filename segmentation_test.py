import easygui
from Unet_Model import UNetModel
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from scipy import ndimage
from scipy import misc
from utils import splitname
import matplotlib.image as mplimg

root_path = 'D:/Program_Skin_Cancer/'
test_image = easygui.fileopenbox()
filenames_test = splitname(test_image)
print(test_image)
print(filenames_test)
test_image = ndimage.imread(test_image)
print(test_image.shape)
#===============================================================
plt.figure()
plt.imshow(test_image)
#===============================================================
#load weights model seg
model = UNetModel.get_unet_model_seg((128, 128, 3))
model.load_weights(root_path+'OUTPUT/segmentation/model/Unet_weight_segmentation_epochs100_bs4.hdf5')
sample_predictions = model.predict(test_image.reshape((1, 128, 128, 3)))
sample_predictions = sample_predictions.reshape((128, 128))
sample_predictions = sample_predictions > 0.5
sample_predictions = np.array(sample_predictions, dtype=np.uint8)
mplimg.imsave(root_path + "DATA/Data_segmentation/sample/output/segmented_" + filenames_test, sample_predictions, cmap=cm.gray)
#===============================================================
ground_truth_images = sample_predictions

plt.figure()
plt.imshow(ground_truth_images, cmap="gray")
#===============================================================
segmented_images = np.copy(test_image)
for j in range(128):
    for k in range(128):
        for l in range(3):
            segmented_images[j][k][l] = test_image[j][k][l] if ground_truth_images[j][k] == 1 else 0
misc.imsave(root_path + "DATA/Data_segmentation/sample/output/" + filenames_test, segmented_images)
plt.figure()
plt.imshow(segmented_images)
plt.show()