


import cv2
import easygui
from Unet_Model import UNetModel
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from scipy import ndimage
from scipy import misc
from utils import splitname
import matplotlib.image as mplimg

root_path = 'D:/Program_Skin_Cancer/'
test_image = easygui.fileopenbox()
filenames_test = splitname(test_image)
print(test_image)
print(filenames_test)
test_image = ndimage.imread(test_image)
print(test_image.shape)
#===============================================================
plt.figure()
plt.imshow(test_image)
#===============================================================
ground_truth_images = easygui.fileopenbox()
filenames_gt = splitname(ground_truth_images)
ground_truth_images = ndimage.imread(ground_truth_images)
ret, ground_truth_images = cv2.threshold(ground_truth_images, 127, 255, cv2.THRESH_BINARY)
gt_labels_binary = np.array(ground_truth_images)
np.unique(gt_labels_binary[0])
gt_labels_binary = gt_labels_binary / 255
np.unique(gt_labels_binary[0])
mask_train = gt_labels_binary

plt.figure()
plt.imshow(ground_truth_images, cmap="gray")
#===============================================================
segmented_images = np.copy(test_image)
for j in range(128):
    for k in range(128):
        for l in range(3):
            segmented_images[j][k][l] = test_image[j][k][l] if mask_train[j][k] == 1 else 0
misc.imsave(root_path + "DATA/Data_segmentation/sample/segmented_" + filenames_test, segmented_images)
plt.figure()
plt.imshow(segmented_images)
plt.show()