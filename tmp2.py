import pandas as pd
from matplotlib import pyplot as plt
root_path = "D:/Program_Skin_Cancer/OUTPUT/classification/data_train_class_with_model_seg/"
data = pd.read_csv(root_path+'VGG16_logger_classification_epochs100_bs4_.csv')

plt.figure()
plt.plot(data['loss'], color='b', label="training loss")
plt.plot(data['val_loss'], color='r', label="testing loss")
legend = plt.legend(loc='best', shadow=True)
plt.show()